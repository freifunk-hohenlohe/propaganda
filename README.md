Freifunk Hohenlohe Propaganda Material
--------------------------------------

Zum Bearbeiten der Projekt-Daten empfiehlt sich der SVG Editor [Inkscape](http://inkscape.org).
   
_Alle Inhalte stehen unter der folgenden Lizenz:_

[![](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)](https://creativecommons.org/licenses/by-sa/4.0/)
